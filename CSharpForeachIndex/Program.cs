﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpForeachIndex
{
    class Program
    {
        static void Main(string[] args)
        {
            var list = new List<string>() { "つばさ", "こまち", "はやぶさ" };
            foreach (var x in list.Select((value, index) => new { value, index }))
            {
                Console.WriteLine($"index={x.index} value={x.value}");
            }
        }
    }
}
